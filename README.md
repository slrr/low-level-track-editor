Low Level Track Editor
=======

This app is part of the suite designed for the Street Legal Racing: Redline game (https://en.wikipedia.org/wiki/Street_Legal_Racing:_Redline).
The programs and their development is discussed on the vStanced forums (http://vstanced.com/viewtopic.php?f=68&t=13249).

The program can be used to make low level modification to *track-rpks*, mainly by-vertex position modification of *phys* and *poly* objects. The app is based on the SlrrLib (https://gitlab.com/slrr/slrr-lib) project.

## Building

You'll need Visual Studio 2019 or higher to build Low Level Track Editor.

1. Clone this repository
2. Open the Visual Studio solution
3. Select either the target platform and build the solution (needed files will be copied over to the target directory).

## Contributing

Any contributions to the project are welcomed, it's recommended to use GitLab [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

## License

All source code in this repository is licensed under a [BSD 3-clause license](LICENSE.md).