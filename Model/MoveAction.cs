﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LowLevelTrackEditor.Model
{
  public enum ActionType
  {
    MovePolyObject,
    MovePhysObject,
    MovePolyVertex,
    MovePhysVertex
  }

  public struct MoveAction
  {
    public float DeltaX
    {
      get;
      private set;
    }
    public float DeltaY
    {
      get;
      private set;
    }
    public float DeltaZ
    {
      get;
      private set;
    }
    public SlrrLib.Geom.NamedModel Object
    {
      get;
      private set;
    }
    public int VertexIndex
    {
      get;
      private set;
    }
    public ActionType Type
    {
      get;
      private set;
    }

    public MoveAction(float dX,float dY, float dZ, SlrrLib.Geom.NamedModel o,ActionType action,int vertInd = -1)
    {
      DeltaX = dX;
      DeltaY = dY;
      DeltaZ = dZ;
      Object = o;
      VertexIndex = vertInd;
      Type = action;
    }
  }
}
