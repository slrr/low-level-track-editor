﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using SlrrLib.View;

namespace LowLevelTrackEditor
{
  internal enum LoadMethod
  {
    NotLoaded,
    PhysicsObjects,
    PolyObjects,
    LimitedRange
  }

  public partial class MainWindow : Window
  {
    private const float tickForMove = 0.01f;
    private SlrrLib.Geom.MapRpkModelFactory currentFactory;
    private LoadMethod currentLoadMethod = LoadMethod.NotLoaded;
    private Dictionary<SlrrLib.Model.FileEntry, SlrrLib.Model.BinaryNamedSpatialData> physToParent = null;
    private Stack<Model.MoveAction> lastMoved = new Stack<Model.MoveAction>();
    private SlrrLib.Geom.NamedPolyModel chosenPoly = null;
    private SlrrLib.Geom.NamedPhysModel chosenPhys = null;
    private int chosenPolyVertIndex = -1;
    private int chosenPhysVertIndex = -1;
    private float boundDefaultMargin = 100.0f;
    private string rpkFnam = "";
    private string lastOpenFileDialogDir = System.IO.Directory.GetCurrentDirectory();

    public MainWindow()
    {
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        lastOpenFileDialogDir = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      ctrlOrbitingViewport.MarkerMoved += ctrlOrbitingViewport_MarkerlMoved;
    }

    private SlrrLib.Geom.BoundBox3D getBoundsAroundMarker()
    {
      var markerPos = ctrlOrbitingViewport.MarkerPosition * 100.0;
      var diag = new BoundBox3DDialog((float)markerPos.X + boundDefaultMargin, (float)markerPos.Y + boundDefaultMargin, (float)markerPos.Z + boundDefaultMargin,
                                      (float)markerPos.X - boundDefaultMargin, (float)markerPos.Y - boundDefaultMargin, (float)markerPos.Z - boundDefaultMargin);
      if (diag.ShowDialog() == true)
      {
        return new SlrrLib.Geom.BoundBox3D(diag.Bounds);
      }
      return null;
    }
    private string getRpkFileNameFromUser()
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
      if (System.IO.Directory.Exists(lastOpenFileDialogDir))
        dlg.InitialDirectory = lastOpenFileDialogDir;
      else
        dlg.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
      dlg.FileName = "";
      dlg.DefaultExt = ".rpk";
      dlg.Filter = "RPKs|*.rpk";
      var result = dlg.ShowDialog();
      rpkFnam = "";
      if (result == true)
      {
        lastOpenFileDialogDir = System.IO.Path.GetDirectoryName(dlg.FileName);
        System.IO.File.WriteAllText("lastDir", lastOpenFileDialogDir);
        return dlg.FileName;
      }
      return null;
    }
    private void refreshPivots()
    {
      ctrlListOfPivots.Items.Clear();
      foreach (var model in ctrlOrbitingViewport.CurrentModels)
        ctrlListOfPivots.Items.Add(model);
    }
    private SlrrLib.Geom.MapRpkModelFactory getFactoryForLoadType(LoadMethod method)
    {
      SlrrLib.Geom.MapRpkModelFactory tempFactory = null;
      switch (currentLoadMethod)
      {
        case LoadMethod.PhysicsObjects:
          tempFactory = new SlrrLib.Geom.MapRpkModelFactory(rpkFnam, true, false);
          break;
        case LoadMethod.PolyObjects:
          tempFactory = new SlrrLib.Geom.MapRpkModelFactory(rpkFnam, false, true);
          break;
        case LoadMethod.LimitedRange:
          var bounds = getBoundsAroundMarker();
          if (bounds != null)
          {
            tempFactory = new SlrrLib.Geom.MapRpkModelFactory(rpkFnam, true, true, bounds);
          }
          break;
      }
      return tempFactory;
    }
    private void fillPhysLookup(SlrrLib.Geom.MapRpkModelFactory fact)
    {
      foreach (var res in fact.RpkLoaded.RESEntries)
      {
        foreach (var rsd in res.RSD.InnerEntries)
        {
          if (rsd is SlrrLib.Model.BinarySpatialNode)
          {
            foreach (var dat in (rsd as SlrrLib.Model.BinarySpatialNode).DataArray)
            {
              buildPhysToParentDict(dat);
            }
          }
        }
      }
    }
    private void loadSceneRpk(LoadMethod method)
    {
      var rpk = getRpkFileNameFromUser();
      if (rpk != null)
        rpkFnam = rpk;
      else
        return;
      currentLoadMethod = method;
      currentFactory = getFactoryForLoadType(currentLoadMethod);
      if (currentFactory == null)
        return;
      ctrlOrbitingViewport.RenderScene(currentFactory);
      Title = rpkFnam;
      refreshPivots();
      physToParent = new Dictionary<SlrrLib.Model.FileEntry, SlrrLib.Model.BinaryNamedSpatialData>();
      if (currentFactory.LoadPhys)
        fillPhysLookup(currentFactory);
      ctrlOrbitingViewport.AddMarker();
      ctrlOrbitingViewport.SetPolyTexturesFromRPK();
    }
    private void loadSecondaryRpk()
    {
      var rpk = getRpkFileNameFromUser();
      if (rpk != null)
        rpkFnam = rpk;
      else
        return;
      SlrrLib.Geom.MapRpkModelFactory tempFactory = getFactoryForLoadType(currentLoadMethod);
      if (tempFactory == null)
        return;
      ctrlOrbitingViewport.AddToScene(tempFactory);
      refreshPivots();
      if (tempFactory.LoadPhys)
        fillPhysLookup(tempFactory);
    }
    private void buildPhysToParentDict(SlrrLib.Model.BinaryNamedSpatialData parent)
    {
      foreach (var dat in parent.OwnedEntries)
      {
        physToParent[dat] = parent;
        if (dat is SlrrLib.Model.BinarySpatialNode)
        {
          foreach (var innerDat in (dat as SlrrLib.Model.BinarySpatialNode).DataArray)
          {
            physToParent[innerDat] = parent;
            buildPhysToParentDict(innerDat);
          }
        }
      }
    }
    private void fixBoundsFromStart(SlrrLib.Model.FileEntry entr)
    {
      if (physToParent.ContainsKey(entr))
      {
        var parent = physToParent[entr];
        float maxX = 0.0f;
        float maxY = 0.0f;
        float maxZ = 0.0f;
        float minX = 0.0f;
        float minY = 0.0f;
        float minZ = 0.0f;
        if (entr is SlrrLib.Model.BinaryInnerPhysEntry)
        {
          var entrPhys = entr as SlrrLib.Model.BinaryInnerPhysEntry;
          maxX = entrPhys.Vetices.Max(x => x.VertexX);
          maxY = entrPhys.Vetices.Max(x => x.VertexY);
          maxZ = entrPhys.Vetices.Max(x => x.VertexZ);
          minX = entrPhys.Vetices.Min(x => x.VertexX);
          minY = entrPhys.Vetices.Min(x => x.VertexY);
          minZ = entrPhys.Vetices.Min(x => x.VertexZ);
        }
        if (entr is SlrrLib.Model.BinaryNamedSpatialData)
        {
          var entrNamed = entr as SlrrLib.Model.BinaryNamedSpatialData;
          maxX = entrNamed.BoundingBoxX + entrNamed.BoundingBoxHalfWidthX;
          minX = entrNamed.BoundingBoxX - entrNamed.BoundingBoxHalfWidthX;
          maxY = entrNamed.BoundingBoxY + entrNamed.BoundingBoxHalfWidthY;
          minY = entrNamed.BoundingBoxY - entrNamed.BoundingBoxHalfWidthY;
          maxZ = entrNamed.BoundingBoxZ + entrNamed.BoundingBoxHalfWidthZ;
          minZ = entrNamed.BoundingBoxZ - entrNamed.BoundingBoxHalfWidthZ;
        }
        bool outOfParentBound = false;
        if (parent.BoundingBoxHalfWidthX - Math.Abs(parent.BoundingBoxX - maxX) < -0.01)
        {
          outOfParentBound = true;
          parent.BoundingBoxHalfWidthX -= parent.BoundingBoxHalfWidthX - Math.Abs(parent.BoundingBoxX - maxX) - 0.05f;
        }
        if (parent.BoundingBoxHalfWidthX - Math.Abs(parent.BoundingBoxX - minX) < -0.01)
        {
          outOfParentBound = true;
          parent.BoundingBoxHalfWidthX -= parent.BoundingBoxHalfWidthX - Math.Abs(parent.BoundingBoxX - minX) - 0.05f;
        }
        if (parent.BoundingBoxHalfWidthY - Math.Abs(parent.BoundingBoxY - maxY) < -0.01)
        {
          outOfParentBound = true;
          parent.BoundingBoxHalfWidthY -= parent.BoundingBoxHalfWidthY - Math.Abs(parent.BoundingBoxY - maxY) - 0.05f;
        }
        if (parent.BoundingBoxHalfWidthY - Math.Abs(parent.BoundingBoxY - minY) < -0.01)
        {
          outOfParentBound = true;
          parent.BoundingBoxHalfWidthY -= parent.BoundingBoxHalfWidthY - Math.Abs(parent.BoundingBoxY - minY) - 0.05f;
        }
        if (parent.BoundingBoxHalfWidthZ - Math.Abs(parent.BoundingBoxZ - maxZ) < -0.01)
        {
          outOfParentBound = true;
          parent.BoundingBoxHalfWidthZ -= parent.BoundingBoxHalfWidthZ - Math.Abs(parent.BoundingBoxZ - maxZ) - 0.05f;
        }
        if (parent.BoundingBoxHalfWidthZ - Math.Abs(parent.BoundingBoxZ - minZ) < -0.01)
        {
          outOfParentBound = true;
          parent.BoundingBoxHalfWidthZ -= parent.BoundingBoxHalfWidthZ - Math.Abs(parent.BoundingBoxZ - minZ) - 0.05f;
        }
        if (outOfParentBound)
        {
          SlrrLib.Model.MessageLog.AddMessage("Boudn fix on type : " + entr.GetType().Name);
          SlrrLib.Model.MessageLog.AddMessage(" MaxX: " + maxX +
                                              " MaxY: " + maxY +
                                              " MaxZ: " + maxZ +
                                              " MinX: " + minX +
                                              " MinY: " + minY +
                                              " MinZ: " + minZ);
          fixBoundsFromStart(parent);
        }
      }
    }
    private void setCurrentTextInd()
    {
      if (chosenPoly == null)
        return;
      if (chosenPoly.PolySource != null)
      {
        ctrlTextBlockTextureIndex.Text = chosenPoly.PolySource.Meshes.Select(x => x.TextureIndex.ToString()).Distinct().Aggregate((x, y) => x + "," + y);
      }
    }
    private SlrrLib.Geom.NamedPolyModel getNearestPolyToMarker()
    {
      SlrrLib.Geom.NamedPolyModel closest = null;
      float dist = float.MaxValue;
      var markerPos = ctrlOrbitingViewport.MarkerPosition * 100.0;
      var polyPos = new Vector3D(0, 0, 0);
      float distPot = 0;
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedPolyModel>())
      {
        if (pivotObj == null)
          continue;
        if (pivotObj.PolySource == null)
          continue;
        var mesh = pivotObj.PolySource.Meshes.ElementAt(pivotObj.MeshIndex);
        foreach (var vert in mesh.Vertices)
        {
          polyPos.X = vert.VertexCoordX;
          polyPos.Y = vert.VertexCoordY;
          polyPos.Z = vert.VertexCoordZ;
          distPot = (float)(polyPos - markerPos).LengthSquared;
          if (distPot < dist)
          {
            dist = distPot;
            closest = pivotObj;
          }
        }
      }
      return closest;
    }
    private SlrrLib.Geom.NamedPhysModel getNearestPhysToMarker()
    {
      SlrrLib.Geom.NamedPhysModel closest = null;
      float dist = float.MaxValue;
      var markerPos = ctrlOrbitingViewport.MarkerPosition * 100.0;
      var polyPos = new Vector3D(0, 0, 0);
      float distPot = 0;
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedPhysModel>())
      {
        if (pivotObj == null)
          continue;
        if (pivotObj.PhysSource == null)
          continue;
        foreach (var second in pivotObj.PhysSource.Vetices)
        {
          polyPos.X = second.VertexX;
          polyPos.Y = second.VertexY;
          polyPos.Z = second.VertexZ;
          distPot = (float)(polyPos - markerPos).LengthSquared;
          if (distPot < dist)
          {
            dist = distPot;
            closest = pivotObj;
          }
        }
      }
      return closest;
    }
    private void movePhysObject(SlrrLib.Geom.NamedPhysModel obj, double deltaX, double deltaY, double deltaZ)
    {
      if(obj == null)
        return;
      if (obj.PhysSource == null)
        return;
      foreach (var second in obj.PhysSource.Vetices)
      {
        second.VertexX += (float)deltaX;
        second.VertexY += (float)deltaY;
        second.VertexZ += (float)deltaZ;
      }
      obj.ModelGeom.Geometry = SlrrLib.Geom.InnerPhysGeometry.GetMeshFromModelData(obj.PhysSource);
      ctrlOrbitingViewport.RecalculatePivot(obj);
    }
    private void movePolyObject(SlrrLib.Geom.NamedPolyModel obj, double deltaX, double deltaY, double deltaZ)
    {
      if(obj == null)
        return;
      if (obj.PolySource == null)
        return;
      foreach (var mesh in obj.PolySource.Meshes)
      {
        foreach (var vert in mesh.Vertices)
        {
          vert.VertexCoordX += (float)deltaX;
          vert.VertexCoordY += (float)deltaY;
          vert.VertexCoordZ += (float)deltaZ;
        }
      }
      obj.ModelGeom.Geometry = SlrrLib.Geom.InnerPolyGeometry.GetMeshFromModelData(obj.PolySource.Meshes.ElementAt(obj.MeshIndex));
      ctrlOrbitingViewport.RecalculatePivot(obj);
    }
    private int getNearestPhysVertIndexToMarker(SlrrLib.Geom.NamedPhysModel obj)
    {
      if(obj == null)
        return -1;
      if (obj.PhysSource == null)
        return -1;
      var markerPos = ctrlOrbitingViewport.MarkerPosition * 100.0;
      int vertIndex = -1;
      float dist = float.MaxValue;
      float distPot = float.MaxValue;
      var vertPos = new Vector3D(0, 0, 0);
      int c = obj.PhysSource.Vetices.Count();
      for (int i = 0; i != c; ++i)
      {
        vertPos.X = obj.PhysSource.Vetices.ElementAt(i).VertexX;
        vertPos.Y = obj.PhysSource.Vetices.ElementAt(i).VertexY;
        vertPos.Z = obj.PhysSource.Vetices.ElementAt(i).VertexZ;
        distPot = (float)(vertPos - markerPos).LengthSquared;
        if (distPot < dist)
        {
          dist = distPot;
          vertIndex = i;
        }
      }
      return vertIndex;
    }
    private int getNearestPolyVertIndexToMarker(SlrrLib.Geom.NamedPolyModel obj)
    {
      if(obj == null)
        return -1;
      if (obj.PolySource == null)
        return -1;
      int meshIndex = obj.MeshIndex;
      float dist = float.MaxValue;
      float distPot = float.MaxValue;
      int vertIndex = -1;
      var vertPos = new Vector3D(0, 0, 0);
      var markerPos = ctrlOrbitingViewport.MarkerPosition * 100.0;
      int mesh_c = obj.PolySource.Meshes.Count();
      int mesh_i = obj.MeshIndex;
      int vert_c = obj.PolySource.Meshes.ElementAt(mesh_i).VerticesCount;
      for (int vert_i = 0; vert_i != vert_c; ++vert_i)
      {
        vertPos.X = obj.PolySource.Meshes.ElementAt(mesh_i).Vertices.ElementAt(vert_i).VertexCoordX;
        vertPos.Y = obj.PolySource.Meshes.ElementAt(mesh_i).Vertices.ElementAt(vert_i).VertexCoordY;
        vertPos.Z = obj.PolySource.Meshes.ElementAt(mesh_i).Vertices.ElementAt(vert_i).VertexCoordZ;
        distPot = (float)(vertPos - markerPos).LengthSquared;
        if (distPot < dist)
        {
          dist = distPot;
          vertIndex = vert_i;
        }
      }
      return vertIndex;
    }
    private void movePhysVerticesInBound(SlrrLib.Geom.BoundBox3D box, double deltaX, double deltaY, double deltaZ, SlrrLib.Geom.NamedPhysModel obj = null)
    {
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedPhysModel>())
      {
        if (obj != null && obj != pivotObj)
          continue;
        if (pivotObj == null)
          continue;
        if (pivotObj.PhysSource == null)
          continue;
        bool meshUpdated = false;
        int vert_i = 0;
        foreach (var vert in pivotObj.PhysSource.Vetices)
        {
          if (box.IsPositionInside(vert.VertexX, vert.VertexY, vert.VertexZ))
          {
            movePhysVertex(pivotObj, vert_i, (float)deltaX, (float)deltaY, (float)deltaZ, false);
            meshUpdated = true;
          }
          vert_i++;
        }
        if (meshUpdated)
        {
          pivotObj.ModelGeom.Geometry = SlrrLib.Geom.InnerPhysGeometry.GetMeshFromModelData(pivotObj.PhysSource);
          ctrlOrbitingViewport.RecalculatePivot(pivotObj);
        }
      }
    }
    private void movePolyVerticesInBound(SlrrLib.Geom.BoundBox3D box, double deltaX, double deltaY, double deltaZ, SlrrLib.Geom.NamedPolyModel obj = null)
    {
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedPolyModel>())
      {
        if (obj != null && obj != pivotObj)
          continue;
        if (pivotObj == null)
          continue;
        if (pivotObj.PolySource == null)
          continue;
        bool meshUpdated = false;
        int vert_i = 0;
        var mesh = pivotObj.PolySource.Meshes.ElementAt(pivotObj.MeshIndex);
        foreach (var vert in mesh.Vertices)
        {
          if (box.IsPositionInside(vert.VertexCoordX, vert.VertexCoordY, vert.VertexCoordZ))
          {
            vert.VertexCoordX += (float)deltaX;
            vert.VertexCoordY += (float)deltaY;
            vert.VertexCoordZ += (float)deltaZ;
            meshUpdated = true;
          }
          vert_i++;
        }
        if (meshUpdated)
        {
          pivotObj.ModelGeom.Geometry = SlrrLib.Geom.InnerPolyGeometry.GetMeshFromModelData(mesh);
          ctrlOrbitingViewport.RecalculatePivot(pivotObj);
        }
      }
    }
    private void movePhysVertex(SlrrLib.Geom.NamedPhysModel obj, int vertInd, double deltaX, double deltaY, double deltaZ, bool refreshGeom = true)
    {
      if(obj == null)
        return;
      if (obj.PhysSource == null)
        return;
      var phys = obj.PhysSource;
      phys.Vetices.ElementAt(vertInd).VertexX += (float)deltaX;
      phys.Vetices.ElementAt(vertInd).VertexY += (float)deltaY;
      phys.Vetices.ElementAt(vertInd).VertexZ += (float)deltaZ;
      fixBoundsFromStart(phys);
      foreach (var first in phys.FacingProperties)
      {
        if (first.TriIndex0 == vertInd ||
            first.TriIndex1 == vertInd ||
            first.TriIndex2 == vertInd)
        {
          // normal needs recalc
          Vector3D one = new Vector3D(phys.Vetices.ElementAt(first.TriIndex0).VertexX,
                                      phys.Vetices.ElementAt(first.TriIndex0).VertexY,
                                      phys.Vetices.ElementAt(first.TriIndex0).VertexZ);
          Vector3D two = new Vector3D(phys.Vetices.ElementAt(first.TriIndex1).VertexX,
                                      phys.Vetices.ElementAt(first.TriIndex1).VertexY,
                                      phys.Vetices.ElementAt(first.TriIndex1).VertexZ);
          Vector3D three = new Vector3D(phys.Vetices.ElementAt(first.TriIndex2).VertexX,
                                        phys.Vetices.ElementAt(first.TriIndex2).VertexY,
                                        phys.Vetices.ElementAt(first.TriIndex2).VertexZ);
          var nrom = Vector3D.CrossProduct(two - one, three - one);
          nrom.Normalize();
          first.NormalX = (float)nrom.X;
          first.NormalY = (float)nrom.Y;
          first.NormalZ = (float)nrom.Z;
        }
      }
      if (refreshGeom)
      {
        obj.ModelGeom.Geometry = SlrrLib.Geom.InnerPhysGeometry.GetMeshFromModelData(obj.PhysSource);
        ctrlOrbitingViewport.RecalculatePivot(obj);
      }
    }
    private void movePolyVertex(SlrrLib.Geom.NamedPolyModel obj, int vertInd, double deltaX, double deltaY, double deltaZ, bool refreshGeom = true)
    {
      if(obj == null)
        return;
      if (obj.PolySource == null)
        return;
      var mesh = obj.PolySource.Meshes.ElementAt(obj.MeshIndex);
      mesh.Vertices.ElementAt(vertInd).VertexCoordX += (float)deltaX;
      mesh.Vertices.ElementAt(vertInd).VertexCoordY += (float)deltaY;
      mesh.Vertices.ElementAt(vertInd).VertexCoordZ += (float)deltaZ;
      if (refreshGeom)
      {
        obj.ModelGeom.Geometry = SlrrLib.Geom.InnerPolyGeometry.GetMeshFromModelData(mesh);
        ctrlOrbitingViewport.RecalculatePivot(obj);
      }
    }
    private void moveProperPoly(double deltaX, double deltaY, double deltaZ, bool makeUndoEntry = false)
    {
      var poly = chosenPoly;
      if (poly == null)
        poly = getNearestPolyToMarker();
      var mesh = poly.PolySource.Meshes.ElementAt(poly.MeshIndex);
      if (ctrlCheckBoxOnlyMoveSegment.IsChecked == true)
      {
        var closestVert = chosenPolyVertIndex;
        if (closestVert == -1)
          closestVert = getNearestPolyVertIndexToMarker(poly);
        if (ctrlCheckBoxMoveNearSegment.IsChecked == true)
        {
          float margin = UIUtil.ParseOrZero(ctrlTextBoxMarginForVertex.Text);
          float X = mesh.Vertices.ElementAt(closestVert).VertexCoordX;
          float Y = mesh.Vertices.ElementAt(closestVert).VertexCoordY;
          float Z = mesh.Vertices.ElementAt(closestVert).VertexCoordZ;
          SlrrLib.Geom.BoundBox3D boxBound = new SlrrLib.Geom.BoundBox3D(X + margin, Y + margin, Z + margin, X - margin, Y - margin, Z - margin);
          if (closestVert != -1)
          {
            if (ctrlCheckBoxRestrictToOneObj.IsChecked == true)
            {
              movePolyVerticesInBound(boxBound, deltaX, deltaY, deltaZ, poly);
            }
            else
            {
              movePolyVerticesInBound(boxBound, deltaX, deltaY, deltaZ);
            }
          }
        }
        else
        {
          if (makeUndoEntry)
            lastMoved.Push(new Model.MoveAction((float)deltaX, (float)deltaY, (float)deltaZ, poly, Model.ActionType.MovePolyVertex, closestVert));
          movePolyVertex(poly, closestVert, deltaX, deltaY, deltaZ);
        }
      }
      else
      {
        if (makeUndoEntry)
          lastMoved.Push(new Model.MoveAction((float)deltaX, (float)deltaY, (float)deltaZ, poly, Model.ActionType.MovePolyObject));
        movePolyObject(poly, deltaX, deltaY, deltaZ);
      }
    }
    private void moveProperPhys(double deltaX, double deltaY, double deltaZ, bool makeUndoEntry = false)
    {
      var phys = chosenPhys;
      if (phys == null)
        phys = getNearestPhysToMarker();
      if (ctrlCheckBoxOnlyMoveSegment.IsChecked == true)
      {
        var closestVert = chosenPhysVertIndex;
        if (closestVert == -1)
          closestVert = getNearestPhysVertIndexToMarker(phys);
        if (ctrlCheckBoxMoveNearSegment.IsChecked == true)
        {
          float margin = UIUtil.ParseOrZero(ctrlTextBoxMarginForVertex.Text);
          float X = phys.PhysSource.Vetices.ElementAt(closestVert).VertexX;
          float Y = phys.PhysSource.Vetices.ElementAt(closestVert).VertexY;
          float Z = phys.PhysSource.Vetices.ElementAt(closestVert).VertexZ;
          SlrrLib.Geom.BoundBox3D boxBound = new SlrrLib.Geom.BoundBox3D(X + margin, Y + margin, Z + margin, X - margin, Y - margin, Z - margin);
          if (closestVert != -1)
          {
            if (ctrlCheckBoxRestrictToOneObj.IsChecked == true)
            {
              movePhysVerticesInBound(boxBound, deltaX, deltaY, deltaZ, phys);
            }
            else
            {
              movePhysVerticesInBound(boxBound, deltaX, deltaY, deltaZ);
            }
          }
        }
        else
        {
          if (makeUndoEntry)
            lastMoved.Push(new Model.MoveAction((float)deltaX, (float)deltaY, (float)deltaZ, phys, Model.ActionType.MovePhysVertex, closestVert));
          movePhysVertex(phys, closestVert, deltaX, deltaY, deltaZ);
        }
      }
      else
      {
        if (makeUndoEntry)
          lastMoved.Push(new Model.MoveAction((float)deltaX, (float)deltaY, (float)deltaZ, phys, Model.ActionType.MovePhysObject));
        movePhysObject(phys, deltaX, deltaY, deltaZ);
      }
    }
    private void moveProperElement(double deltaX, double deltaY, double deltaZ, bool makeUndoEntry = false)
    {
      chooseClosestObjects();
      if (currentLoadMethod == LoadMethod.PhysicsObjects)
      {
        moveProperPhys(deltaX, deltaY, deltaZ, makeUndoEntry);
      }
      else if (currentLoadMethod == LoadMethod.PolyObjects)
      {
        moveProperPoly(deltaX, deltaY, deltaZ, makeUndoEntry);
      }
      else if (currentLoadMethod == LoadMethod.LimitedRange)
      {
        moveProperPhys(deltaX, deltaY, deltaZ, makeUndoEntry);
        moveProperPoly(deltaX, deltaY, deltaZ, makeUndoEntry);
      }
    }
    private void movePolyUV(SlrrLib.Geom.NamedPolyModel obj, int vertInd, double deltaU, double deltaV, int UVnum, bool refreshGeom = true)
    {
      if(obj == null)
        return;
      if (obj.PolySource == null)
        return;
      var mesh = obj.PolySource.Meshes.ElementAt(obj.MeshIndex);
      var vert = mesh.Vertices.ElementAt(vertInd);
      if (UVnum == 1)
      {
        vert.UVChannel1X += (float)deltaU;
        vert.UVChannel1Y += (float)deltaV;
      }
      else if (UVnum == 2)
      {
        vert.UVChannel2X += (float)deltaU;
        vert.UVChannel2Y += (float)deltaV;
      }
      else if (UVnum == 3)
      {
        vert.UVChannel3X += (float)deltaU;
        vert.UVChannel3Y += (float)deltaV;
      }
      if (refreshGeom)
      {
        obj.ModelGeom.Geometry = SlrrLib.Geom.InnerPolyGeometry.GetMeshFromModelData(mesh);
        ctrlOrbitingViewport.RecalculatePivot(obj);
      }
    }
    private void movePolyUVInBound(SlrrLib.Geom.BoundBox3D box, double deltaU, double deltaV, int UVnum, SlrrLib.Geom.NamedPolyModel obj = null)
    {
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedPolyModel>())
      {
        if (obj != null && obj != pivotObj)
          continue;
        if (pivotObj == null)
          continue;
        if (pivotObj.PolySource == null)
          continue;
        bool meshUpdated = false;
        int vert_i = 0;
        var mesh = pivotObj.PolySource.Meshes.ElementAt(pivotObj.MeshIndex);
        foreach (var vert in mesh.Vertices)
        {
          if (box.IsPositionInside(vert.VertexCoordX, vert.VertexCoordY, vert.VertexCoordZ))
          {
            if (UVnum == 1)
            {
              vert.UVChannel1X += (float)deltaU;
              vert.UVChannel1Y += (float)deltaV;
            }
            else if (UVnum == 2)
            {
              vert.UVChannel2X += (float)deltaU;
              vert.UVChannel2Y += (float)deltaV;
            }
            else if (UVnum == 3)
            {
              vert.UVChannel3X += (float)deltaU;
              vert.UVChannel3Y += (float)deltaV;
            }
            meshUpdated = true;
          }
          vert_i++;
        }
        if (meshUpdated)
        {
          pivotObj.ModelGeom.Geometry = SlrrLib.Geom.InnerPolyGeometry.GetMeshFromModelData(mesh);
          ctrlOrbitingViewport.RecalculatePivot(pivotObj);
        }
      }
    }
    private void moveProperUVElement(double deltaU, double deltaV)
    {
      chooseClosestObjects();
      if (currentLoadMethod == LoadMethod.PolyObjects)
      {
        var selectedUv = int.Parse((ctrlComboUVGenChannel.SelectedItem as ComboBoxItem).Content as string);
        var poly = chosenPoly;
        if (poly == null)
          poly = getNearestPolyToMarker();
        var mesh = poly.PolySource.Meshes.ElementAt(poly.MeshIndex);
        if (ctrlCheckBoxOnlyMoveSegment.IsChecked == true)
        {
          var closestVert = chosenPolyVertIndex;
          if (closestVert == -1)
            closestVert = getNearestPolyVertIndexToMarker(poly);
          if (ctrlCheckBoxMoveNearSegment.IsChecked == true)
          {
            float margin = UIUtil.ParseOrZero(ctrlTextBoxMarginForVertex.Text);
            float X = mesh.Vertices.ElementAt(closestVert).VertexCoordX;
            float Y = mesh.Vertices.ElementAt(closestVert).VertexCoordY;
            float Z = mesh.Vertices.ElementAt(closestVert).VertexCoordZ;
            SlrrLib.Geom.BoundBox3D boxBound = new SlrrLib.Geom.BoundBox3D(X + margin, Y + margin, Z + margin, X - margin, Y - margin, Z - margin);
            if (closestVert != -1)
            {
              if (ctrlCheckBoxRestrictToOneObj.IsChecked == true)
              {
                movePolyUVInBound(boxBound, deltaU, deltaV, selectedUv, poly);
              }
              else
              {
                movePolyUVInBound(boxBound, deltaU, deltaV, selectedUv);
              }
            }
          }
          else
          {
            movePolyUV(poly, closestVert, deltaU, deltaV, selectedUv);
          }
        }
      }
    }
    private void undoAction(Model.MoveAction action)
    {
      switch (action.Type)
      {
        case Model.ActionType.MovePolyObject:
          movePolyObject(action.Object as SlrrLib.Geom.NamedPolyModel, -action.DeltaX, -action.DeltaY, -action.DeltaZ);
          break;
        case Model.ActionType.MovePhysObject:
          movePhysObject(action.Object as SlrrLib.Geom.NamedPhysModel, -action.DeltaX, -action.DeltaY, -action.DeltaZ);
          break;
        case Model.ActionType.MovePolyVertex:
          movePolyVertex(action.Object as SlrrLib.Geom.NamedPolyModel, action.VertexIndex, -action.DeltaX, -action.DeltaY, -action.DeltaZ);
          break;
        case Model.ActionType.MovePhysVertex:
          movePhysVertex(action.Object as SlrrLib.Geom.NamedPhysModel, action.VertexIndex, -action.DeltaX, -action.DeltaY, -action.DeltaZ);
          break;
      }
    }
    private float getKeyMoveModifier(float baseMult = 1.0f)
    {
      float deltaMult = 1.0f;
      if (Keyboard.IsKeyDown(Key.LeftShift))
        deltaMult += 10.0f;
      if (Keyboard.IsKeyDown(Key.LeftCtrl))
        deltaMult += 100.0f;
      if (Keyboard.IsKeyDown(Key.LeftAlt))
        deltaMult += 1000.0f;
      return deltaMult * baseMult;
    }
    private void chooseClosestObjects()
    {
      if (currentLoadMethod == LoadMethod.PhysicsObjects)
      {
        if (chosenPhys == null)
        {
          chosenPhys = getNearestPhysToMarker();
          chosenPhysVertIndex = getNearestPhysVertIndexToMarker(chosenPhys);
          ctrlOrbitingViewport.SetAllModelsOpacity(0.7, 0.7);
          ctrlOrbitingViewport.SetModelOpacity(chosenPhys, 1, 1);
        }
      }
      else if (currentLoadMethod == LoadMethod.PolyObjects)
      {
        if (chosenPoly == null)
        {
          chosenPoly = getNearestPolyToMarker();
          chosenPolyVertIndex = getNearestPolyVertIndexToMarker(chosenPoly);
          ctrlOrbitingViewport.SetAllModelsOpacity(0.7, 0.7);
          ctrlOrbitingViewport.SetModelOpacity(chosenPoly, 1, 1);
        }
      }
      else if (currentLoadMethod == LoadMethod.LimitedRange)
      {
        if (chosenPhys == null)
        {
          chosenPhys = getNearestPhysToMarker();
          chosenPhysVertIndex = getNearestPhysVertIndexToMarker(chosenPhys);
          ctrlOrbitingViewport.SetAllModelsOpacity(0.7, 0.7);
          ctrlOrbitingViewport.SetModelOpacity(chosenPhys, 1, 1);
        }
        if (chosenPoly == null)
        {
          chosenPoly = getNearestPolyToMarker();
          chosenPolyVertIndex = getNearestPolyVertIndexToMarker(chosenPoly);
          ctrlOrbitingViewport.SetAllModelsOpacity(0.7, 0.7);
          ctrlOrbitingViewport.SetModelOpacity(chosenPoly, 1, 1);
        }
      }
    }
    private void moveProperToAbyss()
    {
      if (ctrlCheckBoxMoveNearSegment.IsChecked == false)
      {
        moveProperElement(0, -2000, 0, true);
        chooseClosestObjects();
      }
    }
    private void undoMoveToAbyss()
    {
      if (lastMoved.Any())
      {
        var lastAct = lastMoved.Pop();
        undoAction(lastAct);
      }
    }

    private void ctrlOrbitingViewport_MarkerlMoved(object sender, EventArgs e)
    {
      chosenPoly = null;
      chosenPolyVertIndex = -1;
      chosenPhys = null;
      chosenPhysVertIndex = -1;
    }
    private void setKeyUpHandled(object sender, KeyEventArgs e)
    {
      e.Handled = true;
    }
    private void ctrlListOfPivots_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count == 0)
        return;
      var firstSel = e.AddedItems[0] as SlrrLib.Geom.NamedModel;
      ctrlOrbitingViewport.LookAtPosition(firstSel.Translate);
    }
    private void ctrlWindow_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Space)
      {
        moveProperToAbyss();
        e.Handled = true;
      }
      if (e.Key == Key.Back)
      {
        undoMoveToAbyss();
        e.Handled = true;
      }
    }
    private void ctrlButtonGlobalBodyLinePosX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      float deltaMult = getKeyMoveModifier();
      moveProperElement(Math.Sign(e.Delta) * tickForMove * deltaMult, 0, 0);
      e.Handled = true;
    }
    private void ctrlButtonGlobalBodyLinePosY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      float deltaMult = getKeyMoveModifier();
      moveProperElement(0, Math.Sign(e.Delta) * tickForMove * deltaMult, 0);
      e.Handled = true;
    }
    private void ctrlButtonGlobalBodyLinePosZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      float deltaMult = getKeyMoveModifier();
      moveProperElement(0, 0, Math.Sign(e.Delta) * tickForMove * deltaMult);
      e.Handled = true;
    }
    private void ctrlButtonGlobalScrollU_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      float deltaMult = getKeyMoveModifier(0.1f);
      moveProperUVElement(Math.Sign(e.Delta) * tickForMove * deltaMult, 0);
      e.Handled = true;
    }
    private void ctrlButtonGlobalScrollV_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      float deltaMult = getKeyMoveModifier(0.1f);
      moveProperUVElement(0, Math.Sign(e.Delta) * tickForMove * deltaMult);
      e.Handled = true;
    }
    private void ctrlButtonSavePhys_Click(object sender, RoutedEventArgs e)
    {
      if (currentLoadMethod != LoadMethod.NotLoaded)
      {
        currentFactory.RpkLoaded.Save();
      }
    }
    private void ctrlButtonFilterPhysByUp_Click(object sender, RoutedEventArgs e)
    {
      if (currentLoadMethod == LoadMethod.LimitedRange || currentLoadMethod == LoadMethod.PhysicsObjects)
      {
        var remItems = new List<SlrrLib.Geom.NamedModel>();
        foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedPhysModel>())
        {
          if (pivotObj == null)
            continue;
          if (pivotObj.PhysSource == null)
            continue;
          foreach (var first in pivotObj.PhysSource.FacingProperties)
          {
            Vector3D normal = new Vector3D(first.NormalX, first.NormalY, first.NormalZ);
            if (Math.Abs(Vector3D.DotProduct(normal, new Vector3D(0, 1, 0))) > 0.5)
            {
              remItems.Add(pivotObj);
            }
          }
        }
        foreach (var item in remItems)
        {
          ctrlListOfPivots.Items.Remove(item);
          ctrlOrbitingViewport.RemoveModelFromScene(item);
        }
      }
    }
    private void ctrlButtonFlipPhys_Click(object sender, RoutedEventArgs e)
    {
      var curPhys = chosenPhys;
      if (curPhys == null)
        curPhys = getNearestPhysToMarker();
      if (curPhys != null)
      {
        foreach (var first in curPhys.PhysSource.FacingProperties)
        {
          int bak = first.TriIndex0;
          first.TriIndex0 = first.TriIndex2;
          first.TriIndex2 = bak;
          first.NormalX *= -1.0f;
          first.NormalY *= -1.0f;
          first.NormalZ *= -1.0f;
        }
        curPhys.ModelGeom.Geometry = SlrrLib.Geom.InnerPhysGeometry.GetMeshFromModelData(curPhys.PhysSource);
      }
    }
    private void ctrlButtonRenderScenePhys_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk(LoadMethod.PhysicsObjects);
    }
    private void ctrlButtonRenderScenePoly_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk(LoadMethod.PolyObjects);
    }
    private void ctrlButtonRenderSceneLimited_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk(LoadMethod.LimitedRange);
    }
    private void ctrlButtonAddToScene_Click(object sender, RoutedEventArgs e)
    {
      loadSecondaryRpk();
    }
    private void ctrlTextBoxMarginForVertex_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      e.Handled = true;
      if (UIUtil.ParseOrFalse(ctrlTextBoxMarginForVertex.Text, out float cur))
      {
        cur += Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 1.0f : 0.1f);
        ctrlTextBoxMarginForVertex.Text = UIUtil.FloatToString(cur);
      }
    }
    private void ctrlButtonMoveToAbyss_Click(object sender, RoutedEventArgs e)
    {
      moveProperToAbyss();
    }
    private void ctrlButtonUndoMoceToAbyss_Click(object sender, RoutedEventArgs e)
    {
      undoMoveToAbyss();
    }
  }
}
